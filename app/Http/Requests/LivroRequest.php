<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Maiuscula;

class LivroRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required',
            'autor' => ['required', 'string', new Maiuscula],
            'edicao' => 'required',
            'isbn' => 'nullable|numeric',
        ];
    }
}
